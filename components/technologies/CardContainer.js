import React, { useEffect } from 'react'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import Divider from '@material-ui/core/Divider'
import { Hidden } from '@material-ui/core'
import Box from '@material-ui/core/Box'
import MediaCard from './MediaCard'
import 'fontsource-roboto'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'

const useStyles = makeStyles({
  divider: {
    width: '100%',
    maxWidth: '100%'
  }
})
const theme = {
  spacing: 8
}

const containerVariants = {
  hidden: {
    opacity: 0,
    y: '-5vw'
  },
  visible: {
    opacity: 1,
    y: 1,

    transition: {
      duration: 1,
      delay: 1
    }
  }
}
export default function CardContainer () {
  const classes = useStyles()
  const animation = useAnimation()
  const [ref, inView, entry] = useInView({ threshold: 0.1 })

  useEffect(() => {
    if (inView) {
      animation.start('visible')
    } else {
      animation.start('hidden')
    }
  }, [animation, inView])

  const CardInfo = [
    {
      image: '/Assets/web.png',
      title: 'Web Development',
      text:
        ' Services for Website development with  technOLOGY stacks like  React JS, and Next JS   '
    },
    {
      image: '/Assets/mobileapp.png',
      title: 'Mobile Apps',
      text:
        ' Services for Mobile application development for Android, and IOS using  React Native Platfoam.'
    },
    {
      image: '/Assets/mobileapp.png',
      title: 'Data Analytics',
      text:
        ' Our data and analytics services transform hidden system data into interactive reports that highlight key trends and insights about your business.'
    },
    {
      image: '/Assets/pwa.png',
      title: 'PWA',
      text:
        '  Services of Progressive Web Apps  that can be installed on your android and ios mobile devices  and computers  '
    },
    {
      image: '/Assets/wordpress.png',
      title: 'Wordpress',
      text:
        ' Services of wordpress Development  to create  ecommerce ,blogs  bussiness portfolio & POS websites  '
    },
    {
      image: '/Assets/design.png',
      title: 'Designing',
      text:
        ' Services of UI/UX design to help create user-friendly interfaces that enable users to understand how to use software products '
    },
    {
      image: '/Assets/socialmedia.png',
      title: 'Social Media Marketing',
      text:
        ' Services of  Social media marketing using social media platfoamns to connect with you audience to build you brand & increase sales '
    },
    {
      image: '/Assets/ecommerce.png',
      title: 'Ecommerce',
      text:
        ' Services of Ecommerce to digitally transform your bussiness into online stores using modern technology  platfoams'
    },
    {
      image: '/Assets/branding.png',
      title: 'Branding',
      text:
        ' Services of branding by helping you with unique brand names, modern logos  and innovative bussiness slogans'
    }
  ]
  return (
    <motion.div ref={ref} initial='hidden' animate={animation}>
      <div id='tech'>
        <Box ml={2}>
          <Grid container direction='row' justify='center'>
            <Grid item lg={10}>
              <Box
                m={1}
                display='grid'
                justifyContent='center'
                alignItems='center'
              >
                <Hidden mdDown>
                  <Box
                    mt={10}
                    display='grid'
                    justifyContent='center'
                    alignItems='center'
                  >
                    <motion.div variants={containerVariants}>
                      <Typography variant='h3' gutterBottom color='textPrimary'>
                        Technologies{' '}
                        <span style={{ color: '#2BA054' }}> &</span> Services
                      </Typography>
                    </motion.div>
                  </Box>
                  <motion.div variants={containerVariants}>
                    <Typography variant='h6' gutterBottom color='textPrimary'>
                      We believe in delivering premier services with cutting
                      edge and forefront technology
                    </Typography>
                  </motion.div>
                  <Grid container justify='center'></Grid>
                </Hidden>
              </Box>
            </Grid>

            <Grid item xs={12} lg={12}>
              <Box
                mt={5}
                display='grid'
                justifyContent='center'
                alignItems='center'
              >
                <Hidden lgUp>
                  <Box
                    display='grid'
                    justifyContent='center'
                    alignItems='center'
                  >
                    <motion.div
                      initial={{ y: '100vw' }}
                      animate={{ y: 0 }}
                      transition={{ delay: 1 }}
                    >
                      <Typography variant='h4' gutterBottom color='textPrimary'>
                        Our Services
                      </Typography>
                    </motion.div>
                  </Box>
                  <motion.div
                    initial={{ y: '-100vw' }}
                    animate={{ y: 0 }}
                    transition={{ delay: 1, duration: 2 }}
                  >
                    <Typography
                      variant='body1'
                      gutterBottom
                      color='textPrimary'
                      align='center'
                    >
                      We believe in delivering premier services with cutting
                      edge and forefront technology
                    </Typography>
                  </motion.div>
                </Hidden>
              </Box>
            </Grid>
          </Grid>
        </Box>
        <Grid container spacing={2}>
          {CardInfo.map(about => (
            <>
              <Grid item xs={12} sm={6} md={3} lg={4}>
                <MediaCard
                  aboutImage={about.image}
                  aboutTitle={about.title}
                  aboutText={about.text}
                />
              </Grid>
            </>
          ))}
        </Grid>
      </div>
    </motion.div>
  )
}
// nothing has changed here as well

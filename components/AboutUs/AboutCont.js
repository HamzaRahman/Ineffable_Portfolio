import React, { useEffect } from 'react'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import 'fontsource-roboto'
import Box from '@material-ui/core/Box'
import Hidden from '@material-ui/core/Hidden'
import ProfileCardDemo from './About'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
const useStyles = makeStyles({
  divider: {
    width: '100%',
    maxWidth: '100%'
  },
  root: {
    flexGrow: 1
  }
})
const theme = {
  spacing: 8
}
const containerVariants = {
  hidden: {
    opacity: 0
  },
  visible: {
    opacity: 1,

    transition: {
      type: 'tween',
      duration: 1,
      delay: 1
    }
  }
}
export default function AboutCont () {
  const classes = useStyles()
  const animation = useAnimation()
  const [ref, inView, entry] = useInView({ threshold: 0.1 })

  useEffect(() => {
    if (inView) {
      animation.start('visible')
    } else {
      animation.start('')
    }
  }, [animation, inView])

  const AboutInfo = [
    {
      image: '/Assets/Ahsan.jfif',
      title: 'CEO',
      name: 'Ahsan Ansari',
      linked: 'https://www.linkedin.com/in/ahsan-ansari-865466143/',
      facebook: 'https://www.facebook.com/ahsan.ansari.33'
    },
    {
      image: '/Assets/Ali.jfif',
      title: 'Technical Lead',
      name: 'Ali Ansari',
      linked: 'https://www.linkedin.com/in/ali-ansari-116b47a2/',
      facebook: 'https://www.facebook.com/aliwajdan.ansari'
    },
    {
      image: '/Assets/abk.jpg',
      title: ' Manager Operations ',
      name: 'Abdul Basit ',
      linked: 'https://www.linkedin.com/in/abdul-basit-khan-70a893186/',
      facebook: 'https://www.facebook.com/abdul.basit.9212'
    },

    {
      image: '/Assets/hamza.jfif',
      title: 'Backend  Lead',
      name: 'Hamza Rehman ',
      linked: 'https://www.linkedin.com/in/hamza-rahman-01750787/',
      facebook: 'https://www.facebook.com/heman7'
    },
    {
      image: '/Assets/saad.jfif',
      title: 'QA Engineer',
      name: 'Saad Arshad',
      linked: 'https://www.linkedin.com/in/saadarshadmehmood/',
      facebook: 'https://www.facebook.com/saadarshadmehmood'
    }
    // {
    //   image: '/Assets/romanamin.jpg',
    //   title: 'DWH/BI Lead',
    //   name: 'Roman Amin',
    //   linked: 'https://www.linkedin.com/in/roman-amin-397105147/?originalSubdomain=pk',
    //   facebook: 'https://www.facebook.com/roman.amin.5'
    // },
   
   
  ]
  const AboutInfos = [
   
   

    {
      image: '/Assets/aliasad.jpg',
      title: ' Software Engineer',
      name: 'Ali Asad',
      linked: 'https://www.linkedin.com/in/ali-asad-05a0121b9/',
      facebook: 'https://www.facebook.com/profile.php?id=100006208697154'
    },
  
    {
      image: '/Assets/ahmad.jpeg',
      title: ' Software Engineer',
      name: 'Ahmad Noor',
      linked: 'https://www.linkedin.com/in/ali-asad-05a0121b9/',
      facebook: 'https://www.facebook.com/profile.php?id=100006208697154'
    },
  
   

    // {
    //   image: '/Assets/female.png',
    //   title: 'Designer',
    //   name: 'Hafsa Younas',
    //   linked: 'https://www.linkedin.com/in/hafsa-younas-0a4368170/',
    //   facebook: ''
    // },
    {
      image: '/Assets/female.png',
      title: 'Game Engineer',
      name: 'Zahida Shahid',
      linked: 'https://www.linkedin.com/in/zahida-shahid-0462b1152',
      facebook: 'https://www.facebook.com/zahida.shahid.756/'
    },
    {
      image: '/Assets/female.png',
      title: 'Software Engineer',
      name: 'Huda Ahmad',
      linked: '',
      facebook: ''
    },
    {
      image: '/Assets/female.png',
      title: ' UI/UX  Designer',
      name: 'Maryam Ashraf',
      linked: 'https://www.linkedin.com/in/ayesha-munir-a2895b205/',
      facebook: 'https://www.linkedin.com/in/ayesha-munir-a2895b205/'
    }
  ]

  return (
    <motion.div ref={ref} initial='hidden' animate={animation}>
      <div id='team'>
        <Box mt={5}>
          <Grid container spacing={1} justify-content='center' align='center'>
            <Grid item xs={11} sm={12}>
              <Box mt={6}>
                <motion.div variants={containerVariants}>
                  <Typography
                    variant='h3'
                    gutterBottom
                    color='textPrimary'
                    justify-content='center'
                  >
                    Our Team
                  </Typography>
                </motion.div>
              </Box>

              <motion.div variants={containerVariants}>
                <Typography
                  variant='body1'
                  gutterBottom
                  color='textPrimary'
                  justify-content='center'
                >
                  " Achievements of an organization are result of combined
                  efforts of every individual "
                </Typography>
                <Box mb={5} />
              </motion.div>
            </Grid>
          </Grid>
          <Hidden mdDown>
            <Grid container direction='row' spacing={4} justify='center'>
              {AboutInfo.map(about => (
                <Grid item xs={8} md={4} lg={2}>
                  <ProfileCardDemo
                    aboutImage={about.image}
                    aboutTitle={about.title}
                    aboutName={about.name}
                    linked={about.linked}
                    facebook={about.facebook}
                  />
                </Grid>
              ))}
            </Grid>
            <Box m={2} />
            <Grid container direction='row' spacing={4} justify='center'>
              {AboutInfos.map(abouts => (
                <Grid item xs={8} md={4} lg={2}>
                  <ProfileCardDemo
                    aboutImage={abouts.image}
                    aboutTitle={abouts.title}
                    aboutName={abouts.name}
                    linked={abouts.linked}
                    facebook={abouts.facebook}
                  />
                </Grid>
              ))}
            </Grid>
          </Hidden>
          <Hidden lgUp>
            <Grid container direction='row' spacing={4} justify='center'>
              {AboutInfo.map(about => (
                <Grid item xs={6} md={5} lg={2}>
                  <ProfileCardDemo
                    aboutImage={about.image}
                    aboutTitle={about.title}
                    aboutName={about.name}
                    linked={about.linked}
                    facebook={about.facebook}
                  />
                </Grid>
              ))}

              {AboutInfos.map(abouts => (
                <Grid item xs={6} md={5} lg={2}>
                  <ProfileCardDemo
                    aboutImage={abouts.image}
                    aboutTitle={abouts.title}
                    aboutName={abouts.name}
                    linked={abouts.linked}
                    facebook={abouts.facebook}
                  />
                </Grid>
              ))}
            </Grid>
          </Hidden>
        </Box>
      </div>
    </motion.div>
  )
}

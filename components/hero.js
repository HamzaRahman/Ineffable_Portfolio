import React, { useEffect, useState, useRef } from 'react'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
// import 'fontsource-roboto'

import Typography from '@material-ui/core/Typography'

import Box from '@material-ui/core/Box'
import Divider from '@material-ui/core/Divider'
import Button from '@material-ui/core/Button'
import { Hidden } from '@material-ui/core'
import { useInView } from 'react-intersection-observer'
import { motion, AnimatePresence, useAnimation } from 'framer-motion'
const line1 = 'Deliver Innovative IT Solutions for Technology insights'
const line2 = 'Inventing better Future'
const line3 =
  'We Are a Team of software developers, Highly Dedicated to develop Products that Matter'
const sentence = {
  hidden: { opacity: 1 },
  visible: {
    opacity: 1,
    transition: {
      delay: 0.5,
      staggerChildren: 0.05
    }
  }
}
const letter = {
  hidden: { opacity: 0, y: 50 },
  visible: {
    opacity: 1,
    y: 0
  }
}
const containerVariants = {
  hidden: {
    opacity: 0,
    y: '-100vw'
  },
  visible: {
    opacity: 1,
    y: 0,
    transition: {
      type: 'tween',
      duration: 1,
      delay: 1
    }
  }
}

const childVariants = {
  hidden: {
    opacity: 0,
    x: '100vw'
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: {
      type: 'tween',
      duration: 1,
      delay: 1
    }
  }
}
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  bg: {
    backgroundColor: '#F5F5F5'
  }
}))
const theme = {
  spacing: 2
}

export default function Hero () {
  const classes = useStyles()
  const animation = useAnimation()
  const [ref, inView, entry] = useInView({ threshold: 0.1 })

  useEffect(() => {
    if (inView) {
      animation.start('visible')
    } else {
      animation.start('')
    }
  }, [animation, inView])
  const container = useRef(null)
  // useEffect(() => {
  //   lottie.loadAnimation({
  //     container: container.current,
  //     render: 'svg',
  //     loop: true,
  //     autoplay: true,
  //     animationData: require('./animations/hero.json'),
  //   })
  // }, [])
  const [showTitle, setShowTitle] = useState(false)
  setTimeout(() => {
    setShowTitle(true)
  }, 2000)
  return (
    <motion.div ref={ref} initial='hidden' animate={animation}>
      <div className={classes.root} id='hero'>
        <motion.div>
          <Grid container direction='row' justify='center' alignItems='center'>
            <Grid item lg={5}>
              <Hidden mdDown>
                <Box mt={15}>
                  <motion.div
                    variants={sentence}
                    initial='hidden'
                    animate='visible'
                  >
                    <Typography variant='h4' gutterBottom color='textPrimary'>
                      {/* Deliver{' '}
                      <span style={{ color: '#2BA054' }}>
                        {' '}
                        Innovative IT Solutions
                      </span>{' '}
                      for Technology insights */}
                      {line1.split('').map((char, index) => {
                        return (
                          <motion.span
                            key={char + '-' + index}
                            variants={letter}
                          >
                            {char}
                          </motion.span>
                        )
                      })}
                    </Typography>
                  </motion.div>
                </Box>
              </Hidden>

              {/* this is for medium and small size screens large and above would be hidden */}
              <Hidden lgUp>
                <Box mt={5} />
                <Box align='center'>
                  <Typography variant='h4' gutterBottom color='textPrimary'>
                    Innovative Software Company
                  </Typography>{' '}
                  <motion.img
                    src='/Assets/herolayer.png'
                    style={{ width: '80%', height: 'auto' }}
                    variants={containerVariants}
                  />
                </Box>
              </Hidden>
              {/* this is for large size screen md and down would be hidden */}
              <Hidden mdDown>
                <motion.div
                  variants={sentence}
                  initial='hidden'
                  animate='visible'
                >
                  <Typography
                    variant='h5'
                    component='h2'
                    gutterBottom
                    color='textSecondary'
                    align='justify'
                  >
                    {line2.split('').map((char, index) => {
                      return (
                        <motion.span key={char + '-' + index} variants={letter}>
                          {char}
                        </motion.span>
                      )
                    })}
                  </Typography>
                </motion.div>

                <Typography
                  variant='subtitle1'
                  gutterBottom
                  color='textPrimary'
                  align='justify'
                >
                  <motion.div
                    variants={sentence}
                    initial='hidden'
                    animate='visible'
                  >
                    {line3.split('').map((char, index) => {
                      return (
                        <motion.span key={char + '-' + index} variants={letter}>
                          {char}
                        </motion.span>
                      )
                    })}
                  </motion.div>
                </Typography>

                {/*   <Button variant='contained' color='primary'>
                Explore More
              </Button>
         
              */}
                <Box mt={2}>
                  <Grid item lg={12}>
                    <motion.img
                      src='/Assets/herolayer.png'
                      style={{ width: '100%' }}
                      variants={childVariants}
                    />
                  </Grid>
                </Box>
              </Hidden>
            </Grid>

            <Grid item xs={12} md={10} lg={6}>
              <motion.img
                src='/Assets/hero-img.png'
                style={{ width: '100%', marginTop: '50px' }}
                exit={{ opacity: 0 }}
                variants={childVariants}
              />
            </Grid>
            <Hidden lgUp>
              <Grid item xs={11} md={10}>
                <Typography
                  variant='h5'
                  gutterBottom
                  color='textSecondary'
                  align='center'
                >
                  We Are here to give Best Services
                </Typography>
                <Typography
                  variant='subtitle1'
                  gutter
                  color='textPrimary'
                  align='center'
                  paragraph
                >
                  We Are a Team of software developers. Highly Dedicated to
                  develop Products that Matter
                </Typography>
                <Grid container justify='center'>
                  {/* <Button variant='contained' color='primary' paragraph justify='center'>
                  Explore More
                </Button> */}
                </Grid>
              </Grid>
            </Hidden>
          </Grid>
        </motion.div>
      </div>
    </motion.div>
  )
}
